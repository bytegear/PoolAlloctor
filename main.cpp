#include <iostream>
#include <mpool.h>

using namespace std;

int main() {
  using namespace mpool;
  using mpool::free;

  uint8_t* a = calloc<uint8_t>();
  uint16_t* b = calloc<uint16_t>();
  uint32_t* c = calloc<uint32_t>();
  void* first_pool_ptr = a;

  *a = 5;
  *b = 10;
  *c = 20;

  a = recalloc<uint8_t>(a, 8192);

  cout << int(*a + *b + *c) << endl;
  cout << msize (a) << endl;

  free(a);
  free(b);
  free(c);

  cout << mpool::msize(first_pool_ptr) << endl;
  return 0;
}
