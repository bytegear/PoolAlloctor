TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        mpool.cpp \
	main.cpp

HEADERS += \
    mpool.h

QMAKE_LFLAGS = -Wl,-enable-stdcall-fixup -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc
QMAKE_LFLAGS_EXCEPTIONS_ON = -mthreads

# Static-link
LIBS += -static C:\QtCreator\Tools\mingw730_64\x86_64-w64-mingw32\lib\libwinpthread-1.dll \
    -static-libstdc++ -static-libgcc

#RESOURCES += qml.qrc

