#include "mpool.h"
#include <cstring>
#include <cmath>

namespace mpool {

extern const mpool::MBlock nullmem(0, true);

MBlock::MBlock(const mpool::MBlock& other)
  : size(other.size), is_free(other.is_free), is_last(other.is_last)
{}

MBlock::MBlock(const size_t size, const bool is_last, const bool is_free)
  : size(size), is_free(is_free), is_last(is_last)
{}

MBlock& MBlock::next() {
  return *reinterpret_cast<MBlock*>(reinterpret_cast<byte*>(memory()) + size);
}

void MBlock::defrag() {
  if(is_last) return;
  MBlock& next_block = next();
  next_block.defrag();
  if(next_block.is_free && is_free) {
    size += next_block.size + sizeof(MBlock);
    is_last = next_block.is_last;
    return;
  } else return;
}

MBlock& MBlock::cut(const size_t size) {
  if(!is_free) return NULLBLOCK;
  if(this->size == size) return *this;
  else if(this->size < (size + sizeof(MBlock))) return NULLBLOCK;
  else {
    new(reinterpret_cast<byte*>(memory()) + size) MBlock(this->size - size - sizeof (MBlock), is_last, true);
    is_last = false;
    is_free = false;
    this->size = size;
    return *this;
  }
}

Pool::Pool(size_t size) {new(&first_block()) MBlock(size, true);}

MBlock& Pool::mfind(const size_t size) {
  MBlock* block = &first_block();
  for(;!block->isLast (); block = &block->next ())
    if(block->isFree()) {
      block = &block->cut(size);
      if(*block != nullmem) return *block;
    }
  if(block->isFree()) {
    block = &block->cut(size);
    if(*block != nullmem) return *block;
  }
  if(next != nullptr)
    return next->mfind(size);
  else {
    if(size > Pool::pool_memory_size)
      addPool (size);
      else addPool();
    return next->mfind(size);
  }
}

void* malloc(const size_t size){
  sizeTest(size);
  if(Pool::first == nullptr)
    addPool();
  MBlock& block = Pool::first->mfind(size);
  return block.memory();
}

void free(void* pointer) {
  reinterpret_cast<MBlock*>(reinterpret_cast<byte*>(pointer) - sizeof (MBlock))->free();
  Pool::first->defrag();
}

void* calloc(const size_t count, const size_t size) {
  if(Pool::first == nullptr)
    addPool();
  MBlock& block = Pool::first->mfind(size * count);
  return block.memory();
}

void* realloc(void* pointer, size_t new_size) {
  if(Pool::first == nullptr)
    addPool();
  MBlock* block = reinterpret_cast<MBlock*>(reinterpret_cast<byte*>(pointer) - sizeof (MBlock));
  if(new_size == block->getSize()) return pointer;
  else if(new_size < block->getSize()) {
    block->free();
    MBlock& new_block = block->cut(new_size);
    Pool::first->defrag();
    if(new_block.memory () != pointer)
      memmove (new_block.memory (), pointer, new_size);
    return new_block.memory();
  } else {
    MBlock& new_block = Pool::first->mfind(new_size);
    memmove (new_block.memory(), pointer, block->getSize ());
    block->free();
    Pool::first->defrag();
    return new_block.memory();
  }
}

size_t msize(void* pointer) {
  return reinterpret_cast<MBlock*>(reinterpret_cast<byte*>(pointer) - sizeof (MBlock))->getSize ();
}

void addPool(size_t pool_size) {
  void* pool = std::malloc(pool_size + sizeof(Pool) + sizeof (MBlock));
  new(pool) Pool(pool_size);
  if(Pool::first == nullptr) {
    Pool::first = reinterpret_cast<Pool*>(pool);
    Pool::last = reinterpret_cast<Pool*>(pool);
  } else {
    Pool::last->setNext(reinterpret_cast<Pool*>(pool));
    Pool::last = reinterpret_cast<Pool*>(pool);
  }
}

void addPool() {
  addPool(Pool::pool_memory_size);
}

void setPoolAllocMemorySize(size_t size) {Pool::pool_memory_size = size;}

bool sizeTest(size_t size) {return size > Pool::pool_memory_size;}

}
