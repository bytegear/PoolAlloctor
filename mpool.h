#ifndef MPOOL_H
#define MPOOL_H

#include <inttypes.h>
#include <new.h>
#include <stdexcept>

namespace mpool {

typedef uint8_t byte;
constexpr size_t pool_size = 4096;

#pragma pack(push, 1)
class MBlock {
  size_t size;
  bool is_free:1;
  bool is_last:1;
public:
  MBlock(const MBlock& other);
  MBlock(const size_t size, const bool is_last, const bool is_free = true);

  bool isFree() {return is_free;}
  bool isLast() {return is_last;}
  size_t getSize() {return size;}

  void* memory() {return reinterpret_cast<byte*>(this) + sizeof (MBlock);}
  MBlock& next();
  MBlock& cut(const size_t size);
  void defrag();
  bool operator==(const MBlock& other) {return this == &other;}
  bool operator!=(const MBlock& other) {return this != &other;}

 void free() {is_free = true;}
};
#pragma pack(pop)

class Pool {
  inline static Pool* first = nullptr;
  inline static Pool* last = nullptr;
  inline static size_t pool_memory_size = 4096;
  Pool* next = nullptr;

  friend bool sizeTest(size_t size);;\
  friend void setPoolAllocMemorySize(size_t size);
  friend void addPool(size_t pool_size);
  friend void addPool();
  friend size_t msize(void* pointer);
  friend void* malloc(const size_t size);
  friend void* calloc(const size_t count, const size_t size);
  friend void* realloc(void* pointer, size_t new_size);
  friend void free(void* pointer);

public:
  Pool(size_t size);
  void setNext(Pool* _next) {
    if(next == nullptr) next = _next;
    else throw std::runtime_error("Try to set next pool to middle of pool-list!");
  }
  MBlock& first_block() {return  *reinterpret_cast<MBlock*>(reinterpret_cast<byte*>(this) + sizeof (Pool));}
  MBlock& mfind(const size_t size);
  void defrag() {
    first_block().defrag();
    if(next != nullptr) next->defrag();
  }
};

bool sizeTest(size_t size);
void setPoolAllocMemorySize(size_t size);
void addPool(size_t pool_size);
void addPool();

size_t msize(void* pointer);
void* malloc(const size_t size);
void* calloc(const size_t count, const size_t size);
void* realloc(void* pointer, size_t new_size);
template <typename type>
type* calloc(const size_t count = 1) {return reinterpret_cast<type*>(calloc(sizeof (type), count));}
template <typename type>
type* recalloc(void* pointer, const size_t new_count = 1) {return reinterpret_cast<type*>(realloc (pointer, sizeof (type) * new_count));}
void free(void* pointer);

}

#define NULLBLOCK *const_cast<mpool::MBlock*>(&nullmem)

#endif // MPOOL_H
