# PoolAlloctor
Simple Pool memory allocator

## Memory allocation functions in ``mpool`` namespace:
* ``void* malloc(const size_t size);`` - allocate ``size`` bytes;
* ``void* calloc(const size_t count, const size_t size);`` - allocate ``count * size`` bytes;
* ``void* realloc(void* pointer, size_t new_size);`` - change memory size of ``pointer`` to ``new_size`` bytes;
* ``template <typename type> type* calloc(const size_t count = 1);`` - allocate ``count * sizeof(type)`` bytes;
* ``template <typename type> type* recalloc(void* pointer, const size_t new_count = 1);`` - change memory size of ``pointer`` to ``new_count * sizeof(type)`` bytes;
* ``void free(void* pointer);`` - free memory block and defrag pool;
* ``size_t msize(void* pointer);`` -  return size of allocated memory;

## Struct of Pool
When an empty pool has the following structure
| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|

### PoolDescriptor (class Pool; size: 4/8 bytes)
* Pool* next (4 bytes on x32 or 8 bytes on x64) - pointer to next pool

### MemoryBlockDescriptor (class MBlock; size: 5/9 bytes)
* size_t size (4 bytes on x32 or 8 bytes on x64) - size of memory
* bool is_free (1 bit) - memory is free
* bool is_last (1 bit) - is last memory block

### MemoryOfBlock (size: MBlock::size bytes)

## Allocation/Free
Allocation of memory divides the memory block into allocated memory and free

### Before allocation:
| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|
|Pool* next = nullptr|size_t size = 4096|*size: 4096 bytes*|
|*size: 4/8 bytes*|bool is_free = true||
||bool is_last = true||
||*size: 5/9 bytes*||
### After ```int* a = reinterpret_cast<int*>(malloc(4))```:

| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|:-:|:-:|
|Pool* next = nullptr|size_t size = 4|*size: 4 bytes*|size_t size = 4087 or 4083|*size: 4087/4083 bytes*|
|*size: 4/8 bytes*|bool is_free = false||bool is_free = true||
||bool is_last = false||bool is_last = true||
||*size: 5/9 bytes*||*size: 5/9 bytes*||

When freeing memory, two free memory blocks are combined into one

### After ```free(a)```:

| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|
|Pool* next = nullptr|size_t size = 4096|*size: 4096 bytes*|
|*size: 4/8 bytes*|bool is_free = true||
||bool is_last = true||
||*size: 5/9 bytes*||

## Allocation of new Pool
When the memory pool is full, the next call to the memory allocation function will allocate a new memory pool

### Before allocaton:
| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|
|Pool* next = nullptr|size_t size = 4096|*size: 4096 bytes*|
|*size: 4/8 bytes*|bool is_free = true||
||bool is_last = true||
||*size: 5/9 bytes*||

### After ``void* buffer = malloc(4096);``:
| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|
|Pool* next = nullptr|size_t size = 4096|*size: 4096 bytes*|
|*size: 4/8 bytes*|bool is_free = false||
||bool is_last = true||
||*size: 5/9 bytes*||

### After ``int* a = reinterpret_cast<int*>(malloc(4))``:
| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|
|Pool* next = *nextpool*|size_t size = 4096|*size: 4096 bytes*|
|*size: 4/8 bytes*|bool is_free = false||
||bool is_last = true||
||*size: 5/9 bytes*||

| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|:-:|:-:|
|Pool* next = nullptr|size_t size = 4|*size: 4 bytes*|size_t size = 4087 or 4083|*size: 4087/4083 bytes*|
|*size: 4/8 bytes*|bool is_free = false||bool is_free = true||
||bool is_last = false||bool is_last = true||
||*size: 5/9 bytes*||*size: 5/9 bytes*||

If the memory allocation function requests more memory than the standard pool size, then the next pool is allocated with the requested memory size.

### After ``void* buffer_2 = malloc(8192);``:
| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|
|Pool* next = *nextpool*|size_t size = 4096|*size: 4096 bytes*|
|*size: 4/8 bytes*|bool is_free = false||
||bool is_last = true||
||*size: 5/9 bytes*||

| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|:-:|:-:|
|Pool* next = *nextpool*|size_t size = 4|*size: 4 bytes*|size_t size = 4087 or 4083|*size: 4087/4083 bytes*|
|*size: 4/8 bytes*|bool is_free = false||bool is_free = true||
||bool is_last = false||bool is_last = true||
||*size: 5/9 bytes*||*size: 5/9 bytes*||

| PoolDescriptor | MemoryBlockDescriptor | MemoryOfBlock |
|:-:|:-:|:-:|
|Pool* next = nullptr|size_t size = 8192|*size: 8192 bytes*|
|*size: 4/8 bytes*|bool is_free = false||
||bool is_last = true||
||*size: 5/9 bytes*||
